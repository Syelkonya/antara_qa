import animal.Animal;
import animal.Carnivorous;
import animal.Herbivore;

import java.util.ArrayList;
import java.util.List;

public class Aviary {

    private int size;
    private List<Animal> animalList = new ArrayList<>();
    private boolean isForCarnivorous;

    public Aviary(int size, boolean isForCarnivorous) {
        this.size = size;
        this.isForCarnivorous = isForCarnivorous;
    }

    public void addAnimal(Animal animal) {
        if (animalList.size() == size)
            System.out.println("Вольер полон");
        else if (animal instanceof Carnivorous &&
                isForCarnivorous) {
            animalList.add(animal);
            System.out.println("Хищник в вольере");
        } else if (animal instanceof Herbivore &&
                !isForCarnivorous) {
            animalList.add(animal);
            System.out.println("Траводяное в вольере");
        } else
            System.out.println("Животное невозможно добавить в вольер");
    }

    public void printAnimalsIdFromAviary(){
        for (Animal animal: animalList)
            System.out.print(animal + " ");
        System.out.println();
    }

}
