import animal.Animal;
import animal.Carnivorous;
import animal.pool.Duck;
import animal.pool.Goat;
import animal.pool.HoneyBadger;
import animal.pool.Lion;
import food.Burger;
import food.WaterMelon;

class Main{
    public static void main(String[] args) {

        Burger burger = new Burger();
        WaterMelon waterMelon = new WaterMelon();

        Lion lion = new Lion();
        lion.eat(burger.isForCarnivorous());
        lion.eat(waterMelon.isForCarnivorous());

        Goat goat = new Goat();
        goat.eat(burger.isForCarnivorous());
        goat.eat(waterMelon.isForCarnivorous());

        Aviary aviaryForCarnivorous = new Aviary(3, true);
        Lion lion2 = new Lion();
        aviaryForCarnivorous.addAnimal(lion);
        aviaryForCarnivorous.addAnimal(lion2);
        aviaryForCarnivorous.addAnimal(goat);
        aviaryForCarnivorous.printAnimalsIdFromAviary();


        Aviary aviaryForHerbivore = new Aviary(5,false);
        aviaryForHerbivore.addAnimal(goat);
        Duck duck = new Duck();
        Duck duck1 = new Duck();
        Duck duck2 = new Duck();
        HoneyBadger honeyBadger = new HoneyBadger();
        aviaryForHerbivore.addAnimal(duck);
        aviaryForHerbivore.addAnimal(duck1);
        aviaryForHerbivore.addAnimal(duck2);
        aviaryForHerbivore.addAnimal(honeyBadger);
        aviaryForHerbivore.printAnimalsIdFromAviary();

    }
}
