package animal;

import food.Food;
import food.Meat;

public class Carnivorous extends Animal {
    @Override
    public void eat(boolean isForCarnivorous) {
        if (isForCarnivorous)
            System.out.println("Принятие пищи прошло успешно");
        else System.out.println("Хищник съел траву, но остался не доволен");
    }
}
