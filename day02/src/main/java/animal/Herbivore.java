package animal;

import food.Food;
import food.Grass;

public class Herbivore extends Animal{

    @Override
    public void eat(boolean isForCarnivorous) {
        if (!isForCarnivorous)
            System.out.println("Принятие пищи прошло успешно");
        else System.out.println("Нельзя накормить травоядное мясом");
    }
}
