package food;

public abstract class Grass extends Food {

    public boolean isForCarnivorous() {
        return false;
    }
}
