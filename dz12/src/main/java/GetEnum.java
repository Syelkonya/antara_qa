import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class GetEnum {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "D:\\DeveloperContent\\chromedriver_win32\\chromedriver.exe");

        //указываю драйверу откуда запускать браузер
       WebDriver driver = new ChromeDriver(new ChromeOptions()
                .setBinary("D:\\Program Files\\Google\\Chrome\\Application\\chrome.exe"));
        WebDriverWait wait = new WebDriverWait(driver, 400);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://www.avito.ru");
        Select categorySelect = new Select(driver.findElement(By.cssSelector("#category")));

        categorySelect.getOptions().forEach(webElement -> {
            System.out.println(webElement.getText().replaceAll(" ","") + "(\"" +
                    webElement.getText() + "\"," + webElement.getAttribute("value") + ")," );
        });
        driver.close();
    }
}
