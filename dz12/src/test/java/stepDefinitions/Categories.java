package stepDefinitions;

public enum Categories {
    ТРАНСПОРТ("ТРАНСПОРТ",1),
    Автомобили("Автомобили",9),
    Мотоциклыимототехника("Мотоциклы и мототехника",14),
    Грузовикииспецтехника("Грузовики и спецтехника",81),
    Водныйтранспорт("Водный транспорт",11),
    Запчастииаксессуары("Запчасти и аксессуары",10),
    НЕДВИЖИМОСТЬ("НЕДВИЖИМОСТЬ",4),
    Квартиры("Квартиры",24),
    Комнаты("Комнаты",23),
    Домадачикоттеджи("Дома, дачи, коттеджи",25),
    Гаражиимашиноместа("Гаражи и машиноместа",85),
    Земельныеучастки("Земельные участки",26),
    Коммерческаянедвижимость("Коммерческая недвижимость",42),
    Недвижимостьзарубежом("Недвижимость за рубежом",86),
    РАБОТА("РАБОТА",110),
    Вакансии("Вакансии",111),
    Резюме("Резюме",112),
    УСЛУГИ("УСЛУГИ",114),
    ЛИЧНЫЕВЕЩИ("ЛИЧНЫЕ ВЕЩИ",5),
    Одеждаобувьаксессуары("Одежда, обувь, аксессуары",27),
    Детскаяодеждаиобувь("Детская одежда и обувь",29),
    Товарыдлядетейиигрушки("Товары для детей и игрушки",30),
    Часыиукрашения("Часы и украшения",28),
    Красотаиздоровье("Красота и здоровье",88),
    ДЛЯДОМАИДАЧИ("ДЛЯ ДОМА И ДАЧИ",2),
    Бытоваятехника("Бытовая техника",21),
    Мебельиинтерьер("Мебель и интерьер",20),
    Посудаитоварыдлякухни("Посуда и товары для кухни",87),
    Продуктыпитания("Продукты питания",82),
    Ремонтистроительство("Ремонт и строительство",19),
    Растения("Растения",106),
    БЫТОВАЯЭЛЕКТРОНИКА("БЫТОВАЯ ЭЛЕКТРОНИКА",6),
    Аудиоивидео("Аудио и видео",32),
    Игрыприставкиипрограммы("Игры, приставки и программы",97),
    Настольныекомпьютеры("Настольные компьютеры",31),
    Ноутбуки("Ноутбуки",98),
    Оргтехникаирасходники("Оргтехника и расходники",99),
    Планшетыиэлектронныекниги("Планшеты и электронные книги",96),
    Телефоны("Телефоны",84),
    Товарыдлякомпьютера("Товары для компьютера",101),
    Фототехника("Фототехника",105),
    ХОББИИОТДЫХ("ХОББИ И ОТДЫХ",7),
    Билетыипутешествия("Билеты и путешествия",33),
    Велосипеды("Велосипеды",34),
    Книгиижурналы("Книги и журналы",83),
    Коллекционирование("Коллекционирование",36),
    Музыкальныеинструменты("Музыкальные инструменты",38),
    Охотаирыбалка("Охота и рыбалка",102),
    Спортиотдых("Спорт и отдых",39),
    ЖИВОТНЫЕ("ЖИВОТНЫЕ",35),
    Собаки("Собаки",89),
    Кошки("Кошки",90),
    Птицы("Птицы",91),
    Аквариум("Аквариум",92),
    Другиеживотные("Другие животные",93),
    Товарыдляживотных("Товары для животных",94),
    ДЛЯБИЗНЕСА("ДЛЯ БИЗНЕСА",8),
    Готовыйбизнес("Готовый бизнес",116),
    Оборудованиедлябизнеса("Оборудование для бизнеса",40);

    private String name;
    private int id;

    Categories(String name,int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
