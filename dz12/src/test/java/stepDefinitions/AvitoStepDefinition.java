package stepDefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.ParameterType;
import io.cucumber.java.ru.И;
import io.cucumber.java.ru.Пусть;
import io.cucumber.java.ru.Тогда;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class AvitoStepDefinition {
    private static WebDriver driver;
    private static WebDriverWait wait;

    private static void waitPlease() {
        try {
            Thread.sleep(5_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Before
    public static void initDriver() {
        System.setProperty("webdriver.chrome.driver", "D:\\DeveloperContent\\chromedriver_win32\\chromedriver.exe");

        //указываю драйверу откуда запускать браузер
        driver = new ChromeDriver(new ChromeOptions()
                .setBinary("D:\\Program Files\\Google\\Chrome\\Application\\chrome.exe"));
        wait = new WebDriverWait(driver, 10);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public static void after() {
        waitPlease();
        driver.close();
    }

    @Пусть("открыт ресурс авито")
    public static void openAvitoWebPage() {
        driver.get("https://www.avito.ru");
    }

    @ParameterType(".*")
    public Categories categories(String category) {
        Categories[] categories = Categories.values();
        for (Categories categoryInEnum : categories) {
            if (categoryInEnum.getName().toUpperCase().contains(category.toUpperCase().trim())) {
                return categoryInEnum;
            }
        }
        return null;
    }

    @И("в выпадающем списке категорий выбрана {categories}")
    public void chooseOfficeEquipmentInDropDownList(Categories categories) {
        Select categorySelect = new Select(driver.findElement(By.cssSelector("#category")));
        categorySelect.selectByVisibleText(categories.getName());
    }


    @И("в поле поиска введено значение {}")
    public void putParameterInSearchField(String parameter) {
        WebElement paramsForSearchAds = wait.until(ExpectedConditions
                .visibilityOfElementLocated(By.xpath("//input[@placeholder='Поиск по объявлениям']")));
        paramsForSearchAds.sendKeys(parameter);
        waitPlease();
    }


    @Тогда("кликнуть по выпадающему списку региона")
    public void clickOnRegionList() {
        driver.findElement(By.xpath("//div[contains(@class,'main-locationWrapper')]")).click();
    }

    @Тогда("в поле регион введено значение {}")
    public void putCityInRegionSearch(String city) {
        driver.findElement(By.xpath("//input[@data-marker = 'popup-location/region/input']"))
                .sendKeys(city);
        waitPlease();

    }

    @И("нажата кнопка показать объявления")
    public void pushButtonToWatchAds() {
        driver.findElement(By.xpath("//button[@data-marker='popup-location/save-button']")).click();
    }

    @Тогда("открылась страница результаты по запросу {}")
    public void openResultsWithParameterRequest(String parameter) {
        String title = driver.findElement(By.xpath("//h1[contains(@class,'page-title-text')]")).getText();

        if (!title.toUpperCase().contains(parameter.toUpperCase())){
            throw new IllegalArgumentException();
        }
    }

    @И("активирован чекбокс только с фотографией")
    public void activateCheckBoxOnlyWithPhoto() {
        WebElement checkbox = driver.findElement(By.xpath("//input[@name='withImagesOnly']"));
        if (!checkbox.isSelected()) {
            checkbox.sendKeys(Keys.SPACE);
        }
        waitPlease();
    }

    @ParameterType(".*")
    public SortParameters sortParameters(String sortParameter) {
        SortParameters[] sortParameters = SortParameters.values();
        for (SortParameters sortParametersInEnum : sortParameters) {
            if (sortParametersInEnum.getName().toUpperCase().contains(sortParameter.toUpperCase().trim())) {
                return sortParametersInEnum;
            }
        }
        return null;
    }

    @И("в выпадающем списке сортировка выбрано значение {sortParameters}")
    public void inDropDownListChooseSortParameter(SortParameters sortParameters) {
        driver.findElement(By.xpath("//div[contains(@class,'form-select-v2')]//option[@value='"
                + sortParameters.getId() + "']")).click();
        waitPlease();
    }


    @И("в консоль выведено значение названия и цены {int} первых товаров")
    public void soutAmountOfFirstProducts(int amount) {
        List<WebElement> webElementList = driver
                .findElements(By.xpath("//div[contains(@class,'snippet-list')]//span[@itemprop='name']"));
        for (int i = 0; i < amount; i++) {
            System.out.println(webElementList.get(i).getText());
        }
    }

}
