package stepDefinitions;

public enum SortParameters {
    ПоУмолчанию("По умолчанию",101),
    Дешевле("Дешевле",1),
    Дороже("Дороже",2),
    ПоДате("По дате",104);

    private String name;
    private int id;

    SortParameters(String name, int id){
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
