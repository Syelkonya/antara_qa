import java.util.List;


public class Pet {
    private Category category;
    private Integer id;
    private String name;
    private List<String> photoUrls;
    private String status;
    private List<Tag> tags;

    public Pet(Category category, Integer id, String name, List<String> photoUrls, String status, List<Tag> tags) {
        this.category = category;
        this.id = id;
        this.name = name;
        this.photoUrls = photoUrls;
        this.status = status;
        this.tags = tags;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setPhotoUrls(List<String> photoUrls) {
        this.photoUrls = photoUrls;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public Category getCategory() {
        return category;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<String> getPhotoUrls() {
        return photoUrls;
    }

    public String getStatus() {
        return status;
    }

    public List<Tag> getTags() {
        return tags;
    }
}
