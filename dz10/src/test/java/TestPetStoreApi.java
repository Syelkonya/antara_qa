import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.util.ArrayList;

import static io.restassured.RestAssured.given;

public class TestPetStoreApi {

    private final String apiUri = "https://petstore.swagger.io/v2/";
    private final Category category = new Category(12, "home");
    private Pet pet;

    @BeforeEach
    void setUp() {
        pet = new Pet(category, 12, "Bublik", new ArrayList<String>(),
                Status.Available.getStatus(), new ArrayList<Tag>());
    }


    @Test
    void shouldAddPetAndReturnStatusSuccess() {

        given()
                .baseUri(apiUri)
                .contentType(ContentType.JSON)
                .body(pet)
                .post("/pet")
                .then()
                .statusCode(200);
    }


    @Test
    void updateExistingPetAndReturnSuccess() {

        pet.setName("To4noNeBublic");

        given()
                .baseUri(apiUri)
                .contentType(ContentType.JSON)
                .body(pet)
                .put("/pet")
                .then()
                .statusCode(200);
    }

    @Test
    void updateExistingPetByPostAndReturnSuccess() {

        pet.setName("To4no");

        given()
                .baseUri(apiUri)
                .contentType(ContentType.JSON)
                .body(pet)
                .post("/pet")
                .then()
                .statusCode(200);
    }


    @Test
    void deletePetByIdAndReturnSuccess(){
        given()
                .baseUri(apiUri)
                .accept(ContentType.JSON)
                .pathParam("petId", pet.getId())
                .when()
                .delete("/pet/{petId}")
                .then()
                .statusCode(200);

        shouldAddPetAndReturnStatusSuccess(); //восстанавливаем животное в облаке
    }


    @Test
    void shouldReturnPetByIdWithSuccessStatus() {

        given()
                .baseUri(apiUri)
                .accept(ContentType.JSON)
                .pathParam("petId", pet.getId())
                .when()
                .get("/pet/{petId}")
                .then()
                .statusCode(200);
    }

    @Test
    void shouldReturnPetByIdWithPetNotFound() {

        given()
                .baseUri(apiUri)
                .accept(ContentType.JSON)
                .pathParam("petId", "122")
                .when()
                .get("/pet/{petId}")
                .then()
                .statusCode(404);
    }

}
