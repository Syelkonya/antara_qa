import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class AvitoFindPrinter {
    public static void main(String[] args) {

        System.setProperty("webdriver.chrome.driver", "D:\\DeveloperContent\\chromedriver_win32\\chromedriver.exe");

        //указываю драйверу откуда запускать браузер
        WebDriver driver = new ChromeDriver(new ChromeOptions()
                .setBinary("D:\\Program Files\\Google\\Chrome\\Application\\chrome.exe"));

        WebDriverWait wait = new WebDriverWait(driver, 400);

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);

        driver.get("https://www.avito.ru");


        //выбор категории
        Select categorySelect = new Select(driver.findElement(By.cssSelector("#category")));
        categorySelect.selectByVisibleText("Оргтехника и расходники");

        //выбор Принтеров
        WebElement paramsForSearchAds = wait.until(ExpectedConditions
                .visibilityOfElementLocated(By.xpath("//input[@placeholder='Поиск по объявлениям']")));
        paramsForSearchAds.sendKeys("Принтер");
        waitPlease();

        //выбор города
        driver.findElement(By.xpath("//div[contains(@class,'main-locationWrapper')]")).click();
        driver.findElement(By.xpath("//input[@data-marker = 'popup-location/region/input']"))
                .sendKeys("Владивосток");
        waitPlease();
        driver.findElement(By.xpath("//li[@data-marker='suggest(0)']")).click();
        driver.findElement(By.xpath("//button[@data-marker='popup-location/save-button']")).click();

        //Поиск только с доставкой
        WebElement checkbox = driver.findElement(By.xpath("//label[@data-marker='delivery-filter']"));
        if (!checkbox.isSelected()) {
            checkbox.sendKeys(Keys.SPACE);
        }
        driver.findElement(By.xpath("//div[contains(@class, 'applyButton-root')]")).click();

        //снкачала дорогие
        driver.findElement(By.xpath("//div[contains(@class,'form-select-v2')]//option[@value='2']")).click();
        waitPlease();

        //вывести названия 3ех самых дорогих
        List<WebElement> webElementList = driver
                .findElements(By.xpath("//div[contains(@class,'snippet-list')]//span[@itemprop='name']"));
        System.out.println(webElementList.get(0).getText());
        System.out.println(webElementList.get(1).getText());
        System.out.println(webElementList.get(2).getText());

        waitPlease();

        //Закрываем текущее окно браузера
        driver.close();
    }

    private static void waitPlease(){
        try {
            Thread.sleep(5_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
