package aviary;

import animal.Animal;
import animal.Carnivorous;
import animal.Herbivore;


import java.util.HashMap;
import java.util.Map;

public class Aviary {

    private final int capacity;
    private Map<String, Animal> animalHashMap = new HashMap<>();
    private final int aviaryType;
    private final AviarySize aviarySize;
    private final String disableToAdd = "Животное невозможно добавить";
    private final String successfullyAdded = "Животное в вольере";

    public static final int FOR_ALL_ANIMALS = 1;
    public static final int FOR_CARNIVOROUS = 2;
    public static final int FOR_HERBIVORE = 3;
    public static final int FOR_SPECIFIC_ANIMAL = 4;
    private String specAnimalClass;


    public Aviary(int capacity, int animalType, AviarySize aviarySize) {
        this.capacity = capacity;
        this.aviaryType = animalType;
        this.aviarySize = aviarySize;
    }

    public String addAnimal(Animal animal) {
        if (isBigEnough() && ableToPutAnimalInAviaryBySize(animal)) {
            switch (aviaryType) {
                case FOR_ALL_ANIMALS:
                    if ((animal instanceof Carnivorous && !ifAnyHerbivoreAreInAviary()) ||
                            (animal instanceof Herbivore && !ifAnyCarnivorousAreInAviary())) {
                        animalHashMap.put(animal.getName(), animal);
                        return successfullyAdded;
                    }
                    break;
                case FOR_CARNIVOROUS:
                    if (animal instanceof Carnivorous) {
                        animalHashMap.put(animal.getName(), animal);
                        return successfullyAdded;
                    }
                    break;
                case FOR_HERBIVORE:
                    if (animal instanceof Herbivore) {
                        animalHashMap.put(animal.getName(), animal);
                        return successfullyAdded;
                    }
                    break;
                case FOR_SPECIFIC_ANIMAL:
                    if (animalHashMap.size() == 0) {
                        specAnimalClass = String.valueOf(animal.getClass());
                        animalHashMap.put(animal.getName(), animal);
                        return successfullyAdded;
                    } else if (specAnimalClass.equals(String.valueOf(animal.getClass()))) {
                        animalHashMap.put(animal.getName(), animal);
                        return successfullyAdded;
                    }
                    break;
            }
        }
        return disableToAdd;
    }

    public boolean ableToPutAnimalInAviaryBySize(Animal animal) {
        return animal.getAviarySize().getSize() <= aviarySize.getSize();
    }

    public String deleteAnimal(String animalName) {
        animalHashMap.remove(animalName);
        return "Животное на свободе";
    }

    public String getAnimalLink(String animalName) {
        return String.valueOf(animalHashMap.get(animalName));
    }

    private boolean isBigEnough() {
        return (animalHashMap.size() != capacity) && (capacity > 0);
    }

    private boolean ifAnyHerbivoreAreInAviary() {
        for (Animal animal : animalHashMap.values()) {
            if (animal instanceof Herbivore) return true;
        }
        return false;
    }

    private boolean ifAnyCarnivorousAreInAviary() {
        for (Animal animal : animalHashMap.values()) {
            if (animal instanceof Carnivorous) return true;
        }
        return false;
    }


}
