package animal.interfaces;

public interface RunAbility {
    String run();
}
