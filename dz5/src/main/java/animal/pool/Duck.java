package animal.pool;

import animal.Herbivore;
import aviary.AviarySize;

public class Duck extends Herbivore {
    public Duck(String name) {
        super(name);
        setAviarySize(AviarySize.XSmall);
    }
}
