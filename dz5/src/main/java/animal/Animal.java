package animal;

import aviary.AviarySize;
import food.Food;

public abstract class Animal {
    private final String name; //уникальный идентификатор
    private AviarySize aviarySize;

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String eat(Food food) throws WrongFoodException {
        return "Чего-то и съел";
    }

    protected void setAviarySize(AviarySize aviarySize) {
        this.aviarySize = aviarySize;
    }

    public AviarySize getAviarySize() {
        return aviarySize;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Animal)) {
            return false;
        }

        Animal animal = (Animal) obj;

        return animal.name.equals(name);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + name.hashCode();
        return result;
    }
}
