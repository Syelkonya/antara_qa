package aviary;

public enum AviarySize {
    Large(4),
    Medium(3),
    Small(2),
    XSmall(1);

    private final int size;

    AviarySize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }
}
