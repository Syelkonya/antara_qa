package aviary;

import animal.Animal;
import animal.Carnivorous;
import animal.Herbivore;


import java.util.HashMap;
import java.util.Map;

public class Aviary {

    private final int capacity;
    private Map<String, Animal> animalHashMap = new HashMap<>();
    private final int aviaryType;
    private final AviarySize aviarySize;

    public static final int FOR_ALL_ANIMALS = 1;
    public static final int FOR_CARNIVOROUS = 2;
    public static final int FOR_HERBIVORE = 3;
    public static final int FOR_SPECIFIC_ANIMAL = 4;
    private String specAnimalClass;


    public Aviary(int capacity, int animalType, AviarySize aviarySize) {
        this.capacity = capacity;
        this.aviaryType = animalType;
        this.aviarySize = aviarySize;
    }

    public void addAnimal(Animal animal) {
        if (isBigEnough()) {
            switch (aviaryType) {
                case FOR_ALL_ANIMALS:
                    if (((animal instanceof Carnivorous && !ifAnyHerbivoreAreInAviary()) ||
                            (animal instanceof Herbivore && !ifAnyCarnivorousAreInAviary()))
                            && ableToPutAnimalInAviaryBySize(animal)) {
                        animalHashMap.put(animal.getName(), animal);
                        System.out.println("Животное в вольере");
                    } else System.out.println("Животное невозможно добавить");
                    break;
                case FOR_CARNIVOROUS:
                    if ((animal instanceof Carnivorous) && ableToPutAnimalInAviaryBySize(animal)) {
                        animalHashMap.put(animal.getName(), animal);
                        System.out.println("Животное в вольере для хищников");
                    } else System.out.println("Животное невозможно добавить");
                    break;
                case FOR_HERBIVORE:
                    if ((animal instanceof Herbivore) && ableToPutAnimalInAviaryBySize(animal)) {
                        animalHashMap.put(animal.getName(), animal);
                        System.out.println("Животное в вольере для ВЕГАНОВ");
                    } else System.out.println("Животное невозможно добавить");
                    break;
                case FOR_SPECIFIC_ANIMAL:
                    if (animalHashMap.size() == 0 && ableToPutAnimalInAviaryBySize(animal)) {
                        specAnimalClass = String.valueOf(animal.getClass());
                        animalHashMap.put(animal.getName(), animal);
                        System.out.println("Животное в вольере для сородичей");
                    } else if (specAnimalClass.equals(String.valueOf(animal.getClass())) && ableToPutAnimalInAviaryBySize(animal)) {
                        animalHashMap.put(animal.getName(), animal);
                        System.out.println("Животное в вольере для сородичей");
                    } else System.out.println("Животное невозможно добавить");
                    break;
            }
        }
    }

    public boolean ableToPutAnimalInAviaryBySize(Animal animal){
        return animal.getAviarySize().getSize() <= aviarySize.getSize();
    }

    public void deleteAnimal(String animalName){
        animalHashMap.remove(animalName);
        System.out.println("Животное на свободе");
    }

    public void getAnimalLink(String animalName){
        System.out.println(animalHashMap.get(animalName));
    }

    private boolean isBigEnough() {
        if (animalHashMap.size() == capacity || capacity == 0) {
            System.out.println("Вольер полон");
            return false;
        } else return true;
    }

    private boolean ifAnyHerbivoreAreInAviary() {
        for (Animal animal : animalHashMap.values()) {
            if (animal instanceof Herbivore) return true;
        }
        return false;
    }

    private boolean ifAnyCarnivorousAreInAviary() {
        for (Animal animal : animalHashMap.values()) {
            if (animal instanceof Carnivorous) return true;
        }
        return false;
    }


}
