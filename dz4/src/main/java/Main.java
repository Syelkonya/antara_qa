import animal.WrongFoodException;
import animal.pool.Goat;
import animal.pool.HoneyBadger;
import animal.pool.Lion;
import aviary.Aviary;
import aviary.AviarySize;
import food.Burger;
import food.WaterMelon;

class Main{
    public static void main(String[] args) {

        Burger burger = new Burger();
        WaterMelon waterMelon = new WaterMelon();

        Lion lion = new Lion("Львович");

        try {
            lion.eat(burger);
        } catch (WrongFoodException e) {
            e.printStackTrace();
        }

        try {
            lion.eat(waterMelon);
        } catch (WrongFoodException e) {
//            e.printStackTrace();
        }

        System.out.println(lion.getName());

        System.out.println();

        Goat goat = new Goat("K.O.");


        System.out.println(goat.getName());

        System.out.println(goat.equals(new Goat("K.O."))); //проверка equals и hashcode

        System.out.println("||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");

        Aviary aviaryForAll = new Aviary(2, Aviary.FOR_CARNIVOROUS, AviarySize.Small);

        Lion lion2 = new Lion("во Львович");
        Goat goat2 = new Goat("Козел");
        Goat goat3 = new Goat("Козлина");
        HoneyBadger honeyBadger = new HoneyBadger("Мёдик");
        HoneyBadger honeyBadger2 = new HoneyBadger("Петрович");

        aviaryForAll.addAnimal(lion);
        aviaryForAll.addAnimal(honeyBadger);
        aviaryForAll.deleteAnimal(honeyBadger.getName());
        aviaryForAll.addAnimal(honeyBadger2);

        System.out.println("||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");

        aviaryForAll.getAnimalLink("Петрович");

        lion.dig();
        lion.run();
        lion.speak();

        goat.dig();
        goat.run();
        goat.speak();

        System.out.println();

        System.out.println(lion.getAviarySize());

    }
}
