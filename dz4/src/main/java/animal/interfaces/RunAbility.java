package animal.interfaces;

public interface RunAbility {
    void run();
}
