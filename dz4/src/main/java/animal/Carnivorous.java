package animal;

import food.Food;
import food.Meat;

public abstract class Carnivorous extends Animal {

    public Carnivorous(String name) {
        super(name);
    }

    @Override
    public void eat(Food food) throws WrongFoodException {
        if (food instanceof Meat) {
            System.out.println("Принятие пищи прошло успешно");
        } else {
            System.out.println("Хищник съел траву, но остался не доволен");
            throw new WrongFoodException();
        }

    }
}
