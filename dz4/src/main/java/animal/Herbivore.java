package animal;

import food.Food;
import food.Grass;

public abstract class Herbivore extends Animal {

    public Herbivore(String name) {
        super(name);
    }

    @Override
    public void eat(Food food) throws WrongFoodException {
        if (food instanceof Grass) {
            System.out.println("Принятие пищи прошло успешно");
        } else {
            System.out.println("Нельзя накормить травоядное мясом");
            throw new WrongFoodException();
        }
    }

}
