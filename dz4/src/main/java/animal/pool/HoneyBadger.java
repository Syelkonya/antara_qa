package animal.pool;

import animal.Carnivorous;
import aviary.AviarySize;

public class HoneyBadger extends Carnivorous {
    public HoneyBadger(String name) {
        super(name);
        setAviarySize(AviarySize.Small);
    }
}
