package aviary;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.List;

@JacksonXmlRootElement(localName = "aviary_pool")
public class AviaryPool {

    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty("aviary")
    private List<Aviary> aviaries;

    public List<Aviary> getAviaries() {
        return aviaries;
    }

    public void setAviaries(List<Aviary> aviaries) {
        this.aviaries = aviaries;
    }
}
