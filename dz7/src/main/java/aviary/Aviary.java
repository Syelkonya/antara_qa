package aviary;

import animal.Animal;
import animal.Carnivorous;
import animal.Herbivore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.HashMap;
import java.util.Map;

public class Aviary {

    private int capacity;


    @JsonProperty("animal_hash_map")
    private Map<String, Animal> animalHashMap = new HashMap<>();

    @JsonProperty("aviary_type")
    private int aviaryType;

    @JsonProperty("aviary_size")
    private AviarySize aviarySize;

    @JsonProperty("special_animal_type")
    private String specAnimalClass;

    private final String DISABLE_TO_ADD = "Животное невозможно добавить";
    private final String SUCCESFULLY_ADDED = "Животное в вольере";

    public static final int FOR_ALL_ANIMALS = 1;
    public static final int FOR_CARNIVOROUS = 2;
    public static final int FOR_HERBIVORE = 3;
    public static final int FOR_SPECIFIC_ANIMAL = 4;


    public Aviary() {
    }

    public Aviary(int capacity, int animalType, AviarySize aviarySize) {
        this.capacity = capacity;
        this.aviaryType = animalType;
        this.aviarySize = aviarySize;
    }

    public String addAnimal(Animal animal) {
        if (isBigEnough() && ableToPutAnimalInAviaryBySize(animal)) {
            switch (aviaryType) {
                case FOR_ALL_ANIMALS:
                    if ((animal instanceof Carnivorous && !ifAnyHerbivoreAreInAviary()) ||
                            (animal instanceof Herbivore && !ifAnyCarnivorousAreInAviary())) {
                        animalHashMap.put(animal.getName(), animal);
                        return SUCCESFULLY_ADDED;
                    }
                    break;
                case FOR_CARNIVOROUS:
                    if (animal instanceof Carnivorous) {
                        animalHashMap.put(animal.getName(), animal);
                        return SUCCESFULLY_ADDED;
                    }
                    break;
                case FOR_HERBIVORE:
                    if (animal instanceof Herbivore) {
                        animalHashMap.put(animal.getName(), animal);
                        return SUCCESFULLY_ADDED;
                    }
                    break;
                case FOR_SPECIFIC_ANIMAL:
                    if (animalHashMap.size() == 0) {
                        specAnimalClass = String.valueOf(animal.getClass());
                        animalHashMap.put(animal.getName(), animal);
                        return SUCCESFULLY_ADDED;
                    } else if (specAnimalClass!= null && specAnimalClass.equals(String.valueOf(animal.getClass()))) {
                        animalHashMap.put(animal.getName(), animal);
                        return SUCCESFULLY_ADDED;
                    }
                    break;
            }
        }
        return DISABLE_TO_ADD;
    }

    public boolean ableToPutAnimalInAviaryBySize(Animal animal) {
        return animal.getAviarySize().getSize() <= aviarySize.getSize();
    }

    public String deleteAnimal(String animalName) {
        animalHashMap.remove(animalName);
        return "Животное на свободе";
    }

    public String getAnimalLink(String animalName) {
        return String.valueOf(animalHashMap.get(animalName));
    }

    private boolean isBigEnough() {
        return (animalHashMap.size() != capacity) && (capacity > 0);
    }

    private boolean ifAnyHerbivoreAreInAviary() {
        for (Animal animal : animalHashMap.values()) {
            if (animal instanceof Herbivore) return true;
        }
        return false;
    }

    private boolean ifAnyCarnivorousAreInAviary() {
        for (Animal animal : animalHashMap.values()) {
            if (animal instanceof Carnivorous) return true;
        }
        return false;
    }


    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public Map<String, Animal> getAnimalHashMap() {
        return animalHashMap;
    }

    public void setAnimalHashMap(Map<String, Animal> animalHashMap) {
        this.animalHashMap = animalHashMap;
    }

    public int getAviaryType() {
        return aviaryType;
    }

    public void setAviaryType(int aviaryType) {
        specAnimalClass = null;
        this.aviaryType = aviaryType;
    }

    public AviarySize getAviarySize() {
        return aviarySize;
    }

    public void setAviarySize(AviarySize aviarySize) {
        this.aviarySize = aviarySize;
    }

    public String getSpecAnimalClass() {
        return specAnimalClass;
    }

    public void setSpecAnimalClass(String specAnimalClass) {
        this.specAnimalClass = specAnimalClass;
    }

}
