import animal.Animal;
import animal.WrongFoodException;
import animal.pool.Duck;
import animal.pool.Goat;
import animal.pool.HoneyBadger;
import animal.pool.Lion;
import aviary.Aviary;
import aviary.AviaryPool;
import aviary.AviarySize;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.BasicPolymorphicTypeValidator;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import food.Burger;
import food.WaterMelon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


class Main {

    private static Logger logger = LoggerFactory.getLogger(Main.class);
    private static AviaryPool aviaryPool;

    public static void main(String[] args) {

        deserializeFromXML("zoo_xml_files/aviaries_before.xml");

        List<Aviary> aviaryList = aviaryPool.getAviaries();

        for (int i = 0; i < aviaryList.size(); i++){
            logger.info("\tAviary # " + i);
            logger.info("\tCapacity: " + aviaryList.get(i).getCapacity());
            logger.info("\tAviary Type: " + aviaryList.get(i).getAviaryType());
            logger.info("\tAviary Size: {}",aviaryList.get(i).getAviarySize());
            aviaryList.get(i).getAnimalHashMap().forEach((key, value) ->
                    logger.info("Кличка: " + key + "\tКодовый номер: " + value));
        }

        Aviary aviaryZero = aviaryList.get(0);
        aviaryZero.setCapacity(3); //увеличили емкость ради медоеда Анатолия
        aviaryZero.setAviaryType(Aviary.FOR_CARNIVOROUS);//свободу медоедам
        logger.info(aviaryZero.addAnimal(new HoneyBadger("Анатолий")));


        Animal va1sya = aviaryZero.getAnimalHashMap().get("Va1sya");
        try {
            logger.info(va1sya.eat(new WaterMelon())); //покормили ВаOneСю дыней
        } catch (WrongFoodException e) {
            logger.error("exception log: ", e);
        }

        Aviary aviaryFirst = aviaryList.get(1);
        logger.info(aviaryFirst.deleteAnimal("Kek"));
        aviaryFirst.setAviarySize(AviarySize.Medium);
        aviaryFirst.setAviaryType(Aviary.FOR_HERBIVORE);
        logger.info(aviaryFirst.addAnimal(new Goat("Петровна")));

        Aviary aviarySecond = new Aviary(2, Aviary.FOR_CARNIVOROUS, AviarySize.Large);
        aviaryList.add(aviarySecond);
        logger.info(aviarySecond.addAnimal(new HoneyBadger("АЙОУ")));

        serializedToXML("zoo_xml_files/aviaries_after.xml");
    }

    private static void serializedToXML(String filePath) {
        try {
            XmlMapper xmlMapper = new XmlMapper();

            String xmlString = xmlMapper.writeValueAsString(aviaryPool);

            File xmlOutput = new File(filePath);
            FileWriter fileWriter = new FileWriter(xmlOutput);
            fileWriter.write(xmlString);
            fileWriter.close();
        } catch (IOException e) {
            logger.error("exception log:", e);
        }
    }

    private static void deserializeFromXML(String filePath) {
        try {
            XmlMapper xmlMapper = new XmlMapper();

            String readContent = new String(Files.readAllBytes(Paths.get(filePath)));

            aviaryPool = xmlMapper.readValue(readContent, AviaryPool.class);
        } catch (IOException e) {
            logger.error("exception log:", e);
        }
    }

}
