package animal;

import animal.pool.Goat;
import animal.pool.Lion;
import aviary.AviarySize;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import food.Food;

import java.util.ArrayList;
import java.util.List;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.EXTERNAL_PROPERTY, property = "@class")
public abstract class Animal {
    private String name; //уникальный идентификатор


    @JsonProperty("aviary_size")
    private AviarySize aviarySize;

    public Animal() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String eat(Food food) throws WrongFoodException {
        return "Чего-то и съел";
    }

    protected void setAviarySize(AviarySize aviarySize) {
        this.aviarySize = aviarySize;
    }

    public AviarySize getAviarySize() {
        return aviarySize;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Animal)) {
            return false;
        }

        Animal animal = (Animal) obj;

        return animal.name.equals(name);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + name.hashCode();
        return result;
    }
}
