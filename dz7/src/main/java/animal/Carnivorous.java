package animal;

import food.Food;
import food.Meat;

public abstract class Carnivorous extends Animal {

    public Carnivorous(String name) {
        super(name);
    }

    protected Carnivorous(){
    }

    @Override
    public String eat(Food food) throws WrongFoodException {
        if (food instanceof Meat) {
            return "Принятие пищи прошло успешно";
        } else {
            throw new WrongFoodException();
        }
    }
}
