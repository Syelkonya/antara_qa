package animal;

import food.Food;
import food.Grass;

public abstract class Herbivore extends Animal {

    public Herbivore(String name) {
        super(name);
    }

    protected Herbivore() {
    }

    @Override
    public String eat(Food food) throws WrongFoodException {
        if (food instanceof Grass) {
            return "Принятие пищи прошло успешно";
        } else {
            throw new WrongFoodException();
        }
    }

}
