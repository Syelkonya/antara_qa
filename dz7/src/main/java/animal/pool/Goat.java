package animal.pool;

import animal.Herbivore;
import animal.interfaces.Digable;
import animal.interfaces.RunAbility;
import animal.interfaces.Speakable;
import aviary.AviarySize;

public class Goat extends Herbivore implements RunAbility, Speakable, Digable {
    public Goat(String name) {
        super(name);
        setAviarySize(AviarySize.Medium);
    }

    public Goat(){}

    @Override
    public String dig() {
        return "пытается что-то вырыть носом";
    }

    @Override
    public String run() {
        return "убегает от волка";
    }

    @Override
    public String speak() {
        return "https://www.youtube.com/watch?v=qj9PbrDhLo8";
    }
}
