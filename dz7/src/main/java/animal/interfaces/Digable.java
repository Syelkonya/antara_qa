package animal.interfaces;

public interface Digable {
    String dig();
}
