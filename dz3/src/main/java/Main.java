import animal.Animal;
import animal.Carnivorous;
import animal.Herbivore;
import animal.pool.Duck;
import animal.pool.Goat;
import animal.pool.HoneyBadger;
import animal.pool.Lion;
import food.Burger;
import food.WaterMelon;
import jdk.nashorn.internal.ir.annotations.Ignore;

class Main{
    public static void main(String[] args){

        Burger burger = new Burger();
        WaterMelon waterMelon = new WaterMelon();

        Lion lion = new Lion("Львович");
        lion.eat(burger);
        lion.eat(waterMelon);
        System.out.println(lion.getName());

        System.out.println();

        Goat goat = new Goat("K.O.");
        goat.eat(burger);
        goat.eat(waterMelon);

        System.out.println(goat.getName());

        System.out.println(goat.equals(new Goat("K.O."))); //проверка equals и hashcode

        System.out.println("-----------------------------------------------------------------");

        Aviary<Herbivore> aviaryForAll = new Aviary<>(2);

        Lion lion2 = new Lion("во Львович");
        Goat goat2 = new Goat("Козел");
        Goat goat3 = new Goat("Козлина");
        HoneyBadger honeyBadger = new HoneyBadger("Мёдик");
        HoneyBadger honeyBadger2 = new HoneyBadger("Петрович");

        aviaryForAll.addAnimal(goat2);
//        aviaryForAll.addAnimal(honeyBadger);
//        Идея не дает возможности добавить
        aviaryForAll.deleteAnimal(goat2.getName());
        aviaryForAll.addAnimal(goat3);


        aviaryForAll.getAnimalLink("Козлина");

        System.out.println("-----------------------------------------------------------------");
        lion.dig();
        lion.run();
        lion.speak();

        goat.dig();
        goat.run();
        goat.speak();
    }
}
