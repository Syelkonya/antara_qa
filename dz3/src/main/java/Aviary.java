import animal.Animal;
import animal.Carnivorous;
import animal.Herbivore;


import java.util.HashMap;
import java.util.Map;

public class Aviary<T extends Animal> {


    private final int size;
    private Map<String, T> animalHashMap = new HashMap<>();

    public Aviary(int size) {
        this.size = size;
    }

    public void addAnimal(T animal) {
        if (isBigEnough() && (animal instanceof Herbivore && !ifAnyCarnivorousAreInAviary()) ||
                (animal instanceof Carnivorous && !ifAnyHerbivoreAreInAviary())) {
            try {
                animalHashMap.put(animal.getName(), animal);
                System.out.println("Животное добавлено в вольер");
            } catch (Exception e) {
                System.out.println("Животное не соответсвует типу вальера");
            }
        } else System.out.println("Животное невозможно добавить");
    }

    public void deleteAnimal(String animalName) {
        animalHashMap.remove(animalName);
        System.out.println("Животное на свободе");
    }

    public void getAnimalLink(String animalName) {
        System.out.println(animalHashMap.get(animalName));
    }

    private boolean isBigEnough() {
        return (animalHashMap.size() < size);
    }

    private boolean ifAnyHerbivoreAreInAviary() {
        for (Animal animal : animalHashMap.values()) {
            if (animal instanceof Herbivore) return true;
        }
        return false;
    }

    private boolean ifAnyCarnivorousAreInAviary() {
        for (Animal animal : animalHashMap.values()) {
            if (animal instanceof Carnivorous) return true;
        }
        return false;
    }


}
