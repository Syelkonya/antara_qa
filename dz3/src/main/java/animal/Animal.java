package animal;

import food.Food;

public abstract class Animal {
    private final String name; //уникальный идентификатор

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void eat(Food food) {
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Animal)) {
            return false;
        }

        Animal animal = (Animal) obj;

        return animal.name.equals(name);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + name.hashCode();
        return result;
    }
}
