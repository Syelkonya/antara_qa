package animal.pool;

import animal.Carnivorous;
import animal.interfaces.Digable;
import animal.interfaces.RunAbility;
import animal.interfaces.Speakable;

public class Lion extends Carnivorous implements RunAbility, Digable, Speakable {

    public Lion(String name) {
        super(name);
    }


    @Override
    public void dig() {
        System.out.println("Лев роет, чтобы закопать кость");
    }

    @Override
    public void run() {
        System.out.println("Лев бежит со скоростью 80 км/ч");
    }

    @Override
    public void speak() {
        System.out.println("Лев " + getName() +" рычит");
    }
}
