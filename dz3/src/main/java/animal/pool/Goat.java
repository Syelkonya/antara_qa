package animal.pool;

import animal.Herbivore;
import animal.interfaces.Digable;
import animal.interfaces.RunAbility;
import animal.interfaces.Speakable;

public class Goat extends Herbivore implements RunAbility, Speakable, Digable {
    public Goat(String name) {
        super(name);
    }

    @Override
    public void dig() {
        System.out.println("пытается что-то вырыть носом");
    }

    @Override
    public void run() {
        System.out.println("убегает от волка");
    }

    @Override
    public void speak() {
        System.out.println("https://www.youtube.com/watch?v=qj9PbrDhLo8");
    }
}
