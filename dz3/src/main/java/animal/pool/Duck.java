package animal.pool;

import animal.Herbivore;

public class Duck extends Herbivore {
    public Duck(String name) {
        super(name);
    }
}
