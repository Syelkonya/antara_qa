package animal.pool;

import animal.Carnivorous;

public class HoneyBadger extends Carnivorous {
    public HoneyBadger(String name) {
        super(name);
    }
}
