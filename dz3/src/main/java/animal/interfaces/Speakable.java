package animal.interfaces;

public interface Speakable {
    void speak();
}
