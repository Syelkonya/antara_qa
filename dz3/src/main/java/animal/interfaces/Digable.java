package animal.interfaces;

public interface Digable {
    void dig();
}
