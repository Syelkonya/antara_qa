import animal.Animal;
import animal.pool.Duck;
import animal.pool.Goat;
import animal.pool.HoneyBadger;
import animal.pool.Lion;
import aviary.Aviary;
import aviary.AviarySize;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;


import java.util.stream.Stream;

import static org.mockito.Mockito.mock;

@DisplayName("Метод addAnimal из класса Aviary должен")
public class AviaryTest {
    private Lion lion;
    private HoneyBadger honeyBadger;
    private Goat goat;
    private Duck duck;
    private final String disableToAdd = "Животное невозможно добавить";
    private final String successfullyAdded = "Животное в вольере";


    @BeforeEach
    void setUp(){
        lion = mock(Lion.class);
        honeyBadger = mock(HoneyBadger.class);
        goat = mock(Goat.class);
        duck = mock(Duck.class);
        lion = new Lion("");
        honeyBadger = new HoneyBadger("");
        goat = new Goat("");
        duck = new Duck("");
    }

    private static Stream<Arguments> generateDataForCheckSizeTest() {
        return Stream.of(
                Arguments.of(1,Aviary.FOR_ALL_ANIMALS, AviarySize.Medium, new Lion("")),
                Arguments.of(1,Aviary.FOR_ALL_ANIMALS, AviarySize.Small, new Goat("")),
                Arguments.of(1,Aviary.FOR_ALL_ANIMALS, AviarySize.XSmall, new HoneyBadger(""))
        );
    }

    @DisplayName("должен возвращать сообщение о невозможности добавить в вольер животное," +
            " которое больше вольера")
    @MethodSource("generateDataForCheckSizeTest")
    @ParameterizedTest
    void shouldReturnStringThatAnimalDisableToAddIfAnimalIsBiggerThanAviary(
            int capacity, int typeOfAviary, AviarySize sizeOfAviary, Animal animal){
        Aviary aviary = new Aviary(capacity, typeOfAviary, sizeOfAviary);
        Assertions.assertEquals(aviary.addAnimal(animal), disableToAdd);
    }

    @DisplayName("должен возвращать сообщение о невозможности добавить в вольер животное," +
            " которое которое не соответсвует типу вольера")
    @Test
    void shouldReturnStringThatAnimalDisableToAddIfAviaryIsForOtherAnimal(){
        Aviary aviary = new Aviary(1,Aviary.FOR_CARNIVOROUS, AviarySize.Large);
        Assertions.assertEquals(aviary.addAnimal(goat), disableToAdd);
        Aviary aviary2 = new Aviary(1,Aviary.FOR_HERBIVORE, AviarySize.Large);
        Assertions.assertEquals(aviary2.addAnimal(lion), disableToAdd);
    }

    @DisplayName("должен возвращать сообщение о невозможности добавить в вольер" +
            " к хищнику травоядное, или наоборот")
    @Test
    void shouldReturnStringThatAnimalDisableToAddIfTryAddHerbivoreToCarnivorous(){
        Aviary aviary = new Aviary(2,Aviary.FOR_ALL_ANIMALS, AviarySize.Large);
        Assertions.assertEquals(aviary.addAnimal(lion), successfullyAdded);
        Assertions.assertEquals(aviary.addAnimal(goat), disableToAdd);
        Aviary aviary2 = new Aviary(2,Aviary.FOR_HERBIVORE, AviarySize.Large);
        Assertions.assertEquals(aviary2.addAnimal(goat), successfullyAdded);
        Assertions.assertEquals(aviary2.addAnimal(lion), disableToAdd);
    }

    @DisplayName("должен возвращать сообщение о невозможности добавить животное в вольер," +
            " еcли вольер полон или его размер меньше нуля или равен ему")
    @Test
    void shouldReturnStringThatAnimalDisableToAddIfAviaryIsFullOrAviarySizeIsBelowZero(){
        Aviary aviary = new Aviary(1,Aviary.FOR_ALL_ANIMALS, AviarySize.Large);
        Assertions.assertEquals(aviary.addAnimal(lion), successfullyAdded);;
        Assertions.assertEquals(aviary.addAnimal(new Lion("1")), disableToAdd);
        Aviary aviary2 = new Aviary(-1,Aviary.FOR_ALL_ANIMALS, AviarySize.Large);
        Assertions.assertEquals(aviary2.addAnimal(lion), disableToAdd);
    }

    @DisplayName("должен возвращать сообщение о невозможности добавить животное в вольер," +
            " при добавлении в вольер животных разного типа")
    @Test
    void shouldReturnStringThatAnimalDisableToAddIfTryToAddAnimalsOfDifferentTypeInAviaryForSpecificAnimal(){
        Aviary aviary = new Aviary(2,Aviary.FOR_SPECIFIC_ANIMAL, AviarySize.Large);
        Assertions.assertEquals(aviary.addAnimal(lion), successfullyAdded);
        Assertions.assertEquals(aviary.addAnimal(honeyBadger), disableToAdd);
    }
}
