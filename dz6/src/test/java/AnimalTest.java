import animal.Carnivorous;
import animal.Herbivore;
import animal.WrongFoodException;
import animal.pool.Goat;
import animal.pool.Lion;
import food.Burger;
import food.Grass;
import food.Meat;
import food.WaterMelon;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Метод eat из класса Animal должен")
public class AnimalTest {
    private Grass waterMelon;
    private Meat burger;
    private Carnivorous carnivorous;
    private Herbivore herbivore;


    @BeforeEach
    void setUp() {
        waterMelon = mock(WaterMelon.class);
        burger = mock(Burger.class);
        carnivorous = new Lion("Carnivorous");
        herbivore = new Goat("Herbivore");
    }


    @DisplayName("возвращать искомое предлжение, когда хищник ест объект, наследующийся от мяса")
    @Test
    void shouldReturnStringIfCarnivorousEatMeat() {
        try {
            Assertions.assertEquals(carnivorous.eat(burger),"Принятие пищи прошло успешно");
        } catch (WrongFoodException e) {
            e.printStackTrace();
        }
    }

    @DisplayName("кидать ошибку, когда хищник ест объект, наследующийся от \"травы\" ")
    @Test
    void shouldThrowExceptionIfCarnivorousTriedToEatGrass() {
        WrongFoodException thrown = assertThrows(
                WrongFoodException.class,
                () -> carnivorous.eat(waterMelon),
                "Expected eat() to throw WrongFoodException, but it didn't"
        );
        assertTrue(thrown.getMessage().contains("Дана неверная еда"));
    }

    @DisplayName("возвращать искомое предлжение, когда травоядное ест объект, наследующийся от травы")
    @Test
    void shouldReturnStringIfHerbivoreEatGrass() {
        try {
            Assertions.assertEquals(herbivore.eat(waterMelon),"Принятие пищи прошло успешно");
        } catch (WrongFoodException e) {
            e.printStackTrace();
        }
    }

    @DisplayName("кидать ошибку, когда травоядное ест объект, наследующийся от мяса")
    @Test
    void shouldThrowExceptionIfHerbivoreTriedToEatMeat() {
        WrongFoodException thrown = assertThrows(
                WrongFoodException.class,
                () -> carnivorous.eat(waterMelon),
                "Expected eat() to throw WrongFoodException, but it didn't"
        );
        assertTrue(thrown.getMessage().contains("Дана неверная еда"));
    }
}
