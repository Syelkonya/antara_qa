import animal.WrongFoodException;
import animal.pool.Goat;
import animal.pool.HoneyBadger;
import animal.pool.Lion;
import aviary.Aviary;
import aviary.AviarySize;
import food.Burger;
import food.WaterMelon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


class Main {

    private static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {


        Burger burger = new Burger();
        WaterMelon waterMelon = new WaterMelon();

        Lion lion = new Lion("Львович");

        try {
            logger.info(lion.eat(burger));
        } catch (WrongFoodException e) {
            logger.error("exception log:", e);
        }

        try {
            logger.info(lion.eat(waterMelon));
        } catch (WrongFoodException e) {
            logger.error("exception log:", e);
        }

        logger.info(lion.getName());

        Goat goat = new Goat("K.O.");

        logger.info("goat name :{}", goat.getName());

        logger.info("check equals and hashcode method {}", goat.equals(new Goat("K.O."))); //проверка equals и hashcode



        Aviary aviaryForAll = new Aviary(2, Aviary.FOR_CARNIVOROUS, AviarySize.Small);

        Lion lion2 = new Lion("во Львович");
        Goat goat2 = new Goat("Козел");
        Goat goat3 = new Goat("Козлина");
        HoneyBadger honeyBadger = new HoneyBadger("Мёдик");
        HoneyBadger honeyBadger2 = new HoneyBadger("Петрович");

        logger.info(aviaryForAll.addAnimal(lion));
        logger.info(aviaryForAll.addAnimal(honeyBadger));
        logger.info(aviaryForAll.deleteAnimal(honeyBadger.getName()));
        logger.info(aviaryForAll.addAnimal(honeyBadger2));

        logger.info("animal link {}",aviaryForAll.getAnimalLink("Петрович"));

        logger.info(lion.dig());
        logger.info(lion.run());
        logger.info(lion.speak());

        logger.info(goat.dig());
        logger.info(goat.run());
        logger.info(goat.speak());


        logger.info("aviary size for Lion {}",lion.getAviarySize());



    }
}
