package animal.pool;

import animal.Animal;
import animal.Carnivorous;
import animal.interfaces.Digable;
import animal.interfaces.RunAbility;
import animal.interfaces.Speakable;
import aviary.AviarySize;

public class Lion extends Carnivorous implements RunAbility, Digable, Speakable {

    public Lion(String name) {
        super(name);
        setAviarySize(AviarySize.Large);
    }

    @Override
    public String dig() {
        return "Лев роет, чтобы закопать кость";
    }

    @Override
    public String run() {
        return "Лев бежит со скоростью 80 км/ч";
    }

    @Override
    public String speak() {
       return "Лев " + getName() +" рычит";
    }
}
