package animal.interfaces;

public interface Speakable {
    String speak();
}
